<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Api
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->bearerToken();

       
        if (!$token || !$user = User::where('auth_token', $token)->first()) {
            return response()->json([
                'message' => 'Auth Token is missing or incorrect'
            ], 401);
        }

        Auth::login($user);

        return $next($request);
    }
}
