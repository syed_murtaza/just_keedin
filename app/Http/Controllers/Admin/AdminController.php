<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DB;
use App\Models\User;
use App\Models\friend;

class AdminController extends Controller
{
    public function show_all_users_blade(){

        $users=User::where('user_type','user')->get();
        return view ('admin.show_all_users',compact('users'));
    }

    public function delete_users(Request $request){

        $id = $request->route('id');
        DB::table('users')->where('id', $id)->delete();
        return redirect('/admin/show_all_users_blade');
    }

    public function edit_users_blade($id){


        $users=User::where('id',$id)->firstOrFail();
        return view ('admin.edit_users',compact('users'));

    }


    public function edit_users(Request $request,$id){
        
        $validator =  Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        if ($validator->fails()) {

            return back()->withInput()->withErrors($validator);
        }
        else{ 
        $user=User::find($id);
        $data = $request-> all();
        $user->update($data);
        return redirect('/admin/show_all_users_blade');

        }

    }

    public function block_users(Request $request)
    {
        $id = $request->route('id');
        
        $user=User::find($id);
        $user->block_status=1;
        $user->save();



 return redirect('/admin/show_all_users_blade');
    }

    public function unblock_users(Request $request)
    {
        $id = $request->route('id');
        
        $user=User::find($id);
        $user->block_status=0;
        $user->save();

        return redirect('/admin/show_all_users_blade');   
    }

    public function show_friends_blade(Request $request){

        $following_id = $request->route('id');
    
        $users=friend::where('following_id',$following_id)->get();
        
        foreach ($users as $user) {
            $follower_id = $user->follower_id;
        }

        $querry = "SELECT users.name, users.id
                    FROM users INNER JOIN friends ON users.id=friends.follower_id WHERE friends.following_id ='$following_id'";
        $result = DB::select($querry);

        // dd($result);

        return view ('admin.show_friends',compact('result'));
    }
}
