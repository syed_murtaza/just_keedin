<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    // public function index()
    // {
    //     return view('home');
    // }

    public function adminHome()
    {
        
        return view('home');
    }
    
    public function chat()
    {

        $apiKey='47275594';
        $apiSecret='87bd2dce0b2d16ca9dd53ede9045f6944a7e7dbf';

         $openTokAPI = new OpenTok($apiKey, $apiSecret);



        $session = $openTokAPI->createSession();




        // Store this sessionId in the database for later use
        $sessionId = $session->getSessionId();

        echo "session".$sessionId;



        if (empty($sessionId)) {
            throw new \Exception("An open tok session could not be created");
                }

        // Generate a Token from just a sessionId (fetched from a database)
        $token = $openTokAPI->generateToken($sessionId);


        $token = $session->generateToken();

        // Set some options in a token
        $token = $session->generateToken(array(
            'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
            'data'       => 'name=Johnny',
            'initialLayoutClassList' => array('focus')
        ));


            return view('chat', [
              'token' => $token,
              'sessionId' => $sessionId
           ]);


   

    }



}
