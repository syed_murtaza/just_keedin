<?php

namespace App\Http\Controllers;
use App\Models\friend;
use App\Models\User;
use App\Models\OnGoingCalls;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use OpenTok\OpenTok;
use DB;


class ApiController extends Controller
{

    public function all(){
       
    }


 public function admin(Request $request){
/*$admin=User::where('id',6)->first();
 $notification = [

            "title" => 'Session and token created',
            "body" =>'join streaming with session id and token',
        ];
            $data = [
    
                "order_id" => "1",

            ];


        $p = $this->proceed_gcm_notifications2($admin->fcm_token, $notification, $data);
dd($p);*/
$query="SELECT * FROM users WHERE LOWER(name) LIKE '%".$request->param."%' ";
//dd($query);
$result = DB::select($query);
dd($result);
//dd(OnGoingCalls::all());

    }

    public function signup(Request $request){

        $validator = \Validator::make($request->all(), [

            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);

    
        if ($validator->fails()) {

            $response = [
                'success' => false,
                'message' => $validator->errors()->first()
            ];

            return response()->json($response);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
	$input['fcm_token']=$request->token;
        $user = User::create($input);
        $user->generateAuthToken();
        $response = [
            'success' => true,
            'message' => 'User Register successfully',
            'user' => $user,
            'status' => 'SIGN_UP_COMPLETE'
        ];

        return response()->json($response);

    }

    public function signin(Request $request)
    {

        $validator = Validator::make($request->all(), [
           'email' => 'required|email',
           'password' => 'required'
       ]);
        
        if ($validator->fails()) {

            $response = [
                'success' => false,
                'message' => $validator->errors()->first()
            ];

            return response()->json($response);
        }
        
        if (Auth::attempt($request->only('email', 'password'))) {
            $user = Auth::user();
            $user->fcm_token=$request->token;
            $user->save();

            if (!$user->auth_token) {
                $user->generateAuthToken();
            }



            $response = [
                'success' => true,
                'message' => 'Signed in successfully',
                'user' => $user
            ];

            return response()->json($response);
        }

        $response = [
            'success' => false,
            'message' => 'Incorrect email or password'
        ];

        return response()->json($response, 401);
    }

    public function proceed_gcm_notifications2($registration_ids, $notification, $data)
    {
        $url    = 'https://fcm.googleapis.com/fcm/send';


        $fields = array(

            'to' => $registration_ids,
            'notification' => $notification,
            'data' => $data

        );

        $headers = array(
            'Authorization:key=AAAA3R05eVg:APA91bE4X7IivrNNetEIYMaIvCisKb-MVRpxnAZVt-AXJGnXgz1Gd8KZfUQhMTEx0QcNgTw84VzH8Z_LoULYBmfTCml0Or-EVXGIjA0-cdPl90U_f-mOEUtScBfWC8jnAT9g-JX7_JZU',
            'Content-Type: application/json',
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === false) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }


public function  Search(Request $request)
    {

	$query="SELECT * FROM users WHERE LOWER(name) LIKE '%".$request->param."%' OR LOWER(email) LIKE '%".$request->param."%' ";

	$result = DB::select($query);

	 $response = [
            'success' => true,
            'data' => $result
        ];

        return response()->json($response);



    }

 public function rejectCall(Request $request)
    {

 $auth=Auth::user();


	$on_going=OnGoingCalls::where('call_id',$request->call_id)->first();

	if(empty($on_going)){
            $response = [
                'success' => true,
                'message' => 'Call Id not found',
            ];
    
            return response()->json($response);
        }

	$notification = [

            "title" => 'REJECTED',
            "body" =>'REJECTED',
        ];

            $data = [
                "call_id"=>'',
                "order_id" =>'',
                "notification_type" => 'REJECTED',
                "Session_id"=>'',
                "Token"=>''

            ];


        $p = $this->proceed_gcm_notifications2($on_going->fcm, $notification, $data);
	$p = $this->proceed_gcm_notifications2($auth->fcm, $notification, $data);
	$on_going->delete();

	$response = [
                'success' => true,
                'message' => 'REJECTED',
            ];
    
            return response()->json($response);

	
	


    }

    public function calling(Request $request)
    {
        
        $auth=Auth::user();



        $API_KEY='47291024';
        $API_SECRET='52ad54bf6dc20561a3cf99afdf11c324ea6ec7af';

        $openTokAPI = new OpenTok($API_KEY, $API_SECRET);

        $session = $openTokAPI->createSession();

        $sessionId = $session->getSessionId();

        echo "session: ".$sessionId;

        if (empty($sessionId)) {
            throw new \Exception("An open tok session could not be created");
                }


        $token = $session->generateToken(array(
            'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
            'data'       => 'name=Johnny',
            'initialLayoutClassList' => array('focus')
        ));

        echo "token".$token;

        $user = User::where('id', $request->user_id)
        ->select(['id', 'name','auth_token','fcm_token'])
        ->first();

        if(empty($user)){
            $response = [
                'success' => true,
                'message' => 'User not found',
            ];
    
            return response()->json($response);
        }


        $notification = [
                    
            "title" => 'Session and token created',
            "body" =>'join streaming with session id and token',
        ];


	$call_id=rand();
        $on_going=new OnGoingCalls();
        $on_going->call_id=$call_id;
        $on_going->to=$user->id;
        $on_going->fcm=$user->fcm_token;
        $on_going->save();

            $data = [
    		"call_id"=>$call_id,
                "order_id" => $request->user_id,
                "notification_type" => 'Session id and token',
                "Session_id"=>$sessionId,
                "Token"=>$token
                    
            ];
          

        $p = $this->proceed_gcm_notifications2($user->fcm_token, $notification, $data);

        $p = $this->proceed_gcm_notifications2($auth->fcm_token, $notification, $data);



         $response = [
            'success' => true,
            'message' => 'SessionId created',
        ] + $data;

        return response()->json($response);

    }

    public function Userlist(){

        $user_id=Auth::user();
        $friends=Auth::user()->friends->pluck('id')->toArray();

    
      
        $response = [
            'users'=>User::all()->except($friends)
        ];
            // User::find($user_id)->friends


        return response()->json($response);
    }

    public function followUser(Request $request)
    {
        User::find($request->id)->followers()->attach([
            'follower_id' => Auth::user()->id
        ]);

        $response = [
            'success' => true,
            'message' => 'Add User as a friend'
        ];

        return response()->json($response);
    }

    public function myFriends(Request $request)
    {
    	$user = Auth::user();

    	$response = [
    		'next_offset' => (int)$request->offset + 20,
            'friends' => $user->following()
            	->whereIn('following_id', $user->followers()->pluck('users.id')->toArray())
                ->select(['users.id', 'name'])
                ->orderBy('friends.created_at', 'desc')
                ->take(20)
                ->offset($request->offset)
                ->get()
    	];

    	return response()->json($response);
    }

    public function getProfile(Request $request)
    {
        $response = [
            'success' => true,
            'user' => User::withCount('friends')
                ->where('id', $request->user_id ?: Auth::user()->id)
                ->first()
        ];

        return response()->json($response);
    }
}
