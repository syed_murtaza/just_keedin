<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'auth_token',
        'fcm_token',
	'block_status',
	'user_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function generateAuthToken() {

        do { 
            $token = Str::random(60);
        } 
        while (User::where('auth_token', $token)->exists());

        $this->auth_token = $token;
        $this->save();
    }

    public function followers()
    {
        return $this->belongsToMany('App\Models\User', 'friends', 'following_id', 'follower_id')->withTimestamps();
    }

    public function following()
    {
        return $this->belongsToMany('App\Models\User', 'friends', 'follower_id', 'following_id')->withTimestamps();
    }

    public function friends(){
        return $this->following()
           ->whereIn('following_id', $this->followers()->pluck('users.id')->toArray());
   }

   public function Notfriends(){
    return $this->following()
       ->whereIn('following_id', $this->followers()->pluck('users.id')->toArray());
}

}
