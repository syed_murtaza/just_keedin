<!doctype html>
<html lang="en">

<head>
	<title>just kidding</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href=" http://157.230.185.155/assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href=" http://157.230.185.155/assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href=" http://157.230.185.155/assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href=" http://157.230.185.155/assets/vendor/chartist/css/chartist-custom.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href=" http://157.230.185.155/assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href=" http://157.230.185.155/assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
    <!-- DATA TABLES -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
   
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="index.html"><img src="assets/img/logo-dark.png" alt="Klorofil Logo" class="img-responsive logo"></a>
			</div>
			<div class="container-fluid">
				
				<form class="navbar-form navbar-left">
					
				</form>
				
				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
						
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span>{{\Auth::user()->name}}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<!-- <li><a href="/admin/home"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li> -->
                                <li> <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="lnr lnr-power-switch"></i>
                                        {{ __('Logout') }}
                                    </a></li>
							</ul>
						</li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
						
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<!-- <li><a href="/admin/home" class="active"><i class="lnr lnr-home"></i> <span>My Profile</span></a></li> -->
						<!-- <li><a href="/admin/add_parents_blade" class=""><i class="lnr lnr-code"></i> <span>Add Parents</span></a></li> -->
						<li><a href="/admin/show_all_users_blade" class=""><i class="lnr lnr-chart-bars"></i> <span>All Users</span></a></li>
						<!-- <li><a href="/admin/payments_blade" class=""><i class="lnr lnr-cog"></i> <span>Payments</span></a></li> -->
						<!-- <li><a href="/admin/all_payments_blade" class=""><i class="lnr lnr-alarm"></i> <span>All Payments</span></a></li> -->
						<!-- <li>
							<a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Pages</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse ">
								<ul class="nav">
									<li><a href="page-profile.html" class="">Profile</a></li>
									<li><a href="page-login.html" class="">Login</a></li>
									<li><a href="page-lockscreen.html" class="">Lockscreen</a></li>
								</ul>
							</div>
						</li>
						<li><a href="tables.html" class=""><i class="lnr lnr-dice"></i> <span>Tables</span></a></li>
						<li><a href="typography.html" class=""><i class="lnr lnr-text-format"></i> <span>Typography</span></a></li>
						<li><a href="icons.html" class=""><i class="lnr lnr-linearicons"></i> <span>Icons</span></a></li> -->
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
                    @yield("body-content")
				</div>
			</div>
			<!-- END MAIN CONTENT -->
            
		</div>
      
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	</div>
    
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="http://157.230.185.155/assets/vendor/jquery/jquery.min.js"></script>
	<script src="http://157.230.185.155/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="http://157.230.185.155/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="http://157.230.185.155/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="http://157.230.185.155/assets/vendor/chartist/js/chartist.min.js"></script>
	<script src="http://157.230.185.155/assets/scripts/klorofil-common.js"></script>
    <script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    @yield("script")
</body>

</html>

