@extends('layouts.app')
@section('content')


<div id="wrapper">
    <div class="vertical-align-wrap">
        <div class="vertical-align-middle">
            <div class="auth-box ">
                <div class="left">
                    <div class="content">
                        <div class="header">
                           
                            <p class="lead">Login to your account</p>
                        </div>
                    <form class="form-auth-small" method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group">

                                <label for="signin-email" class="control-label sr-only">Email</label>

                                <input id="signin-email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>

                        <div class="form-group">

                                <label for="signin-password" class="control-label sr-only">Password</label>

                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>
                        
                     

                      
                            <button type="submit" class="btn btn-primary btn-lg btn-block">
                                    {{ __('Login') }}
                                </button>

                              
                          
                    </form>

                    <!-- <p style="margin-top: 10px">Dont have an account? <a href="/register">Register Here</a></p> -->

                    </div>
                   
                </div>
                
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
	</div>
@endsection
