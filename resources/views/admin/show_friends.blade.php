@extends('layouts.admin_layout')
@section('body-content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> All Followers</h4>
                        
                    </div>
                    <div class="card-body">
                        <input type="hidden" id="_token" value="{{csrf_token()}}">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="text-primary">
                                <th>
                                  #
                                </th>
                                <th>
                                   Follower Name
                                </th>
                                </thead>
                                <tbody>
                                @foreach($result as $user)
                                    <tr>

                                        <td>
                                        {{$user->id}}
                                        </td>

                                        <td>
                                        <a href="{{ URL('/admin/show_friends_blade/'.$user->id )}}" type="button" rel="tooltip">{{$user->name}}</a>
                                        </td>

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection