@extends('layouts.admin_layout')
@section('body-content')



<div id="wrapper">
    <div class="vertical-align-wrap">
        <div class="vertical-align-middle">
            <div class="auth-box ">
                
                    <div class="content">
                        <div class="header">
                           
                            <h3 id="topic">Edit Parents Information</h3>
                        </div>

            
                    <form class="form-auth-small" method="POST" action="{{ URL('/admin/update_users/'.$users->id )}}">
                        @csrf

                        <div class="form-group col-md-6">

                                <label class="control-label">Name</label>

                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name', $users->name) }}" placeholder="Full Name" required autocomplete="name" autofocus>
 
                        </div>

                        <div class="form-group col-md-6">

                                <label class="control-label">Email</label>

                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email', $users->email) }}" name="email" placeholder="Email"  required autocomplete="email" readonly>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        </div>


                      
                            <button type="submit" class="btn btn-primary btn-lg ">
                                    {{ __('Update') }}
                                </button>
                              
                          
                    </form>
                    </div>
                
                
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
	</div>

@endsection
