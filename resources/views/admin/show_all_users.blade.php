@extends('layouts.admin_layout')
@section('body-content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> All Parents</h4>
                        
                    </div>
                    <div class="card-body">
                        <input type="hidden" id="_token" value="{{csrf_token()}}">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="text-primary">
                                <th class="text-center">
                                    #
                                </th>
                                <th>
                                    Full Name
                                </th>
                                <th>
                                   Email
                                </th>
                                <th class="text-center">
                                    User Type
                                </th>
                                <th class="text-center">
                                    Action
                                </th>

                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td class="text-center">
                                        {{$user->id}}
                                        </td>
                                        
                                        <td>
                                        <a href="{{ URL('/admin/show_friends_blade/'.$user->id )}}" type="button" rel="tooltip"
                                              >{{$user->name}}</a>
                                        
                                        </td>
                                        
                                        <td>
                                        {{$user->email}}
                                        </td>
                                        <td class="text-center">
                                        {{$user->user_type}}
                                        </td>
                                        <td class="text-center">

                                        <a href="{{ URL('/admin/edit_users_blade/'.$user->id )}}" type="button" rel="tooltip"
                                               class="btn btn-success btn-icon btn-sm ">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="{{ URL('/admin/delete_users/'.$user->id )}}" type="button" rel="tooltip"
                                               class="btn btn-danger btn-icon btn-sm ">
                                               <i class="fa fa-trash"></i>
                                            </a>
                                            @if($user->block_status == 0)
                                            <a href="{{ URL('/admin/block_users/'.$user->id )}}" type="button" rel="tooltip"
                                               class="btn btn-danger btn-icon btn-sm ">
                                               Block</i>
                                            </a>
                                            @endif

                                            @if($user->block_status == 1)
                                            <a href="{{ URL('/admin/unblock_users/'.$user->id )}}" type="button" rel="tooltip"
                                               class="btn btn-success btn-icon btn-sm ">
                                               Unblock</i>
                                            </a>
                                            @endif

                                        </td>
                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection