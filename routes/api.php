<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/all','ApiController@all')->withoutMiddleware('api')    ;



Route::post('/signup', 'ApiController@signup')->withoutMiddleware('api');
Route::post('/signin', 'ApiController@signin')->withoutMiddleware('api');

Route::post('/calling','ApiController@calling');
Route::post('/reject_call','ApiController@rejectCall');

// get all users listing
Route::get('/getusers','ApiController@UserList');
Route::post('/followuser','ApiController@followUser');
Route::post('/myfriends','ApiController@myFriends');
Route::post('/getProfile','ApiController@getProfile');
Route::post('/admin', 'ApiController@admin')->withoutMiddleware('api');
Route::get('/search','ApiController@Search');
