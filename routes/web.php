<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('optimize:clear');
    Artisan::call('config:clear');
    return "Cache is cleared";
});

Route::get('/', function () {
    if (Auth::user()) {
        if (Auth::user()->user_type == "admin") {
            return redirect('/admin/show_all_users_blade');
        }
        else if (Auth::user()->user_type == "user") {
            return redirect('/login');
        }
        else{
            // return redirect('/logout');
            return redirect('/admin/home');
        }
    } else {
        return view('auth.login');
    }
});

Route::get('admin/home', 'HomeController@adminHome');

Route::group(['prefix'=>'admin','as'=>'admin', 'namespace'=>'Admin'], function () {

   
    Route::get("show_all_users_blade","AdminController@show_all_users_blade")->name('admin.home');
    Route::get("delete_users/{id}","AdminController@delete_users");
    Route::get("edit_users_blade/{id}","AdminController@edit_users_blade");
    Route::post("/update_users/{id}","AdminController@edit_users");
    Route::get("/block_users/{id}","AdminController@block_users");
    Route::get("/unblock_users/{id}","AdminController@unblock_users");
    Route::get("show_friends_blade/{id}","AdminController@show_friends_blade");

});



// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

Route::get('/opentok','HomeController@chat');

require __DIR__.'/auth.php';
